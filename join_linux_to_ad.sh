#!/usr/bin/env bash

# It is simple script for joining linux system to windows Active Directory domain.
# I haven't tested it after the last edit...
# Attention!!! This script can completely broken your system!
# You should not use it if you don't know what you are doing!

# Determining the system in which the script runs.
if [[ `cat /etc/centos-release | cut -d' ' -f4 | cut -d. -f1` = 7 ]]; then
  os="CentOS 7"

elif [[ `cat /etc/centos-release | cut -d' ' -f4 | cut -d. -f1` = 8 ]]; then
  os="CentOS 8"

elif [[ -e /etc/debian_version ]]; then
  os="Ubuntu / Debian"

else
  echo "It isn't known system (Ubuntu/CentOS). I can't continue to work!"
  exit 1

fi

clear # Clear screen.

# DNS settings:
echo
echo "It is simple script for joining linux system to windows Active Directory domain."
echo "Attention!!! This script can completely broken your system!"
echo "You should not use it if you don't know what you are doing!"
echo
echo "You must first set up your internal DNS that knows about your Active Directory."
echo
echo "---------------------"
echo "- Select the action -"
echo "---------------------"
echo
echo '[0] - Stop the script and configure DNS manually.'
echo '[1] - Continue (nothing will work if you do not configure DNS!).'
echo '[2] - Change parameters in /etc/resolv.conf file (if you use NetworkManager, you should chose the next option!).'
echo '[3] - Configure search domain list and DNS with nmcli (NetworkManager must be installed and configured for network manage).'
echo

read -p "[0/1/2/3]: " dnsAction

case "${dnsAction}" in
"1" )
  ;;

"2" )
  echo
  read -p "Enter the search domain(s) space separated: " searchDom
  read -p "Enter IP-address of the first DNS server: " dnsAddr1
  read -p "Enter IP-address of the second DNS server (optional): " dnsAddr2

  if [[ -z ${dnsAddr2} ]]; then
    replace_resolv_conf="search ${searchDom}
nameserver ${dnsAddr1}"
  else
    replace_resolv_conf="search ${searchDom}
nameserver ${dnsAddr1}
nameserver ${dnsAddr2}"
  fi

  cp /etc/resolv.conf /etc/resolv.conf.bak
  echo "${replace_resolv_conf}" > /etc/resolv.conf
  ;;

"3" )
  echo
  nmcli con show
  echo
  read -p "Enter the name of internal interface from the list above (eth0): " interfaceName

  if [[ -z ${interfaceName} ]]; then
    interfaceName="eth0"
  fi

  read -p "Enter the address(es) of DNS server(s) commas separated without spaces: " dnsAddr
  if [[ -z ${dnsAddr} ]]; then
    echo
    echo "You MUST specify address(es) of your internal DNS server(s)! Find out it and come back again!"
    echo
    exit 1
  fi

  read -p "Enter the search domain(s) commas separated, without spaces: " searchDom
  if [[ -z ${searchDom} ]]; then
    echo
    echo "You MUST specify your Active Directory domain! Find out it and come back again!"
    echo
    exit 1
  fi

  nmcli con mod ${interfaceName} ipv4.dns-search ${searchDom} ipv4.dns ${dnsAddr} && nmcli con reload && nmcli con up ${interfaceName}
  ;;

* )
  echo
  echo "Come back again..."
  echo
  exit 1
  ;;

esac

# Installing required packages:
if [[ ${os} == "CentOS 7" ]]; then
  yum -y install krb5-workstation adcli sssd ntp

elif [[ ${os} == "CentOS 8" ]]; then
  dnf -y install krb5-workstation adcli sssd-ad oddjob-mkhomedir chrony
  systemctl enable oddjobd --now

elif [[ ${os} == "Ubuntu / Debian" ]]; then
  apt-get -y install krb5-user adcli sssd ntp

else
  echo
  echo "I don't know this system, sorry..."
  echo
  exit 1

fi

clear # Clear screen.

echo
echo "----------------------------------------"
echo "- Which domain should the system join? -"
echo "----------------------------------------"
echo
echo "[1] - MY.DOMAIN.ORG"
echo "[2] - AD.EXAMPLE.COM"
echo "[3] - Enter manually..."
echo

read -p "[1/2/3]: " join

case "${join}" in
"1" )
  domain="my.domain.org"
  DOMAIN="MY.DOMAIN.ORG"
  ;;

"2" )
  domain="ad.example.com"
  DOMAIN="AD.EXAMPLE.COM"
  ;;

"3" )
  echo
  read -p "Enter the fully qualified domain name: " domain
  domain=`echo ${domain} | sed -e 's/\(.*\)/\L\1/g'`
  DOMAIN=`echo ${domain} | sed -e 's/\(.*\)/\U\1/g'`
  ;;

* )
  echo
  echo "Heresy!"
  echo
  exit 1
  ;;

esac

# Set hostname (get the current domain before the dot and concatinate it with the previously set):
set_hostname=`hostname | sed -r "s/([^.]+)(\..+)$/\1/"`
hostnamectl set-hostname ${set_hostname}.${domain}

echo
read -p "Enter the username who has the right to join the PC to AD: " domAdmin
echo

# Joining the PC to the domain:
adcli join ${domain} -U ${domAdmin}

# Set Kerberos configuration file:
replace_krb5_config="# Configuration snippets may be placed in this directory as well
# includedir /etc/krb5.conf.d/

[logging]
 default = FILE:/var/log/krb5libs.log
 kdc = FILE:/var/log/krb5kdc.log
 admin_server = FILE:/var/log/kadmind.log

[libdefaults]
 dns_lookup_realm = false
 ticket_lifetime = 1d
 renew_lifetime = 7d
 forwardable = true
 rdns = false
 default_realm = ${DOMAIN}
 default_ccache_name = KEYRING:persistent:%{uid}

[realms]
# EXAMPLE.COM = {
#  kdc = kerberos.example.com
#  admin_server = kerberos.example.com
# }

[domain_realm]
# .example.com = EXAMPLE.COM
# example.com = EXAMPLE.COM"
echo "${replace_krb5_config}" > /etc/krb5.conf

# Set SSSD configuration file:
replace_sssd_config="[sssd]
 domains = ${domain}
 config_file_version = 2
 services = nss, pam, pac, sudo

[nss]
 filter_groups = root
 filter_users = root
 reconnection_retries = 3
 entry_cache_nowait_percentage = 75

[pam]

[pac]

[sudo]

[domain/${domain}]
 id_provider = ad
 auth_provider = ad
 access_provider = ad
 dyndns_update = false
 cache_credentials = true
 debug_level = 0
 default_shell = /bin/bash
 fallback_homedir = /home/%f
 ad_gpo_ignore_unreadable = true
# ad_gpo_access_control = enforcing"
echo "${replace_sssd_config}" > /etc/sssd/sssd.conf

# Set sudoers provider:
sed -i -e "/^sudoers:*/s/^/#/" /etc/nsswitch.conf
add_to_nsswitch_config="
sudoers:    files sss"
echo "${add_to_nsswitch_config}" >> /etc/nsswitch.conf

# Setting the rights of SSSD config (without this there will be an error):
chown root:root /etc/sssd/sssd.conf
chmod 600 /etc/sssd/sssd.conf

# Adding SSSD to autostart and restarting it:
if [[ -e /etc/systemd ]]; then
  systemctl enable sssd
  systemctl restart sssd

elif [[ -e /etc/init.d/sssd ]]; then
  chkconfig sssd on
  service sssd restart

else
  echo
  echo "I don't know this init system, sorry..."

fi

# PAM (authorization with SSSD, set "mkhomedir" and username manual input for "lightdm"):
if [[ ${os} == "CentOS 7" ]]; then
  authconfig --enablesssd --enablesssdauth --enablemkhomedir --update

elif [[ ${os} == "CentOS 8" ]]; then
  authselect select sssd with-mkhomedir with-sudo with-faillock --force

elif [[ ${os} == "Ubuntu / Debian" ]]; then
  echo "greeter-show-manual-login=true" >> /usr/share/lightdm/lightdm.conf.d/50-unity-greeter.conf
  sed -i "0,/^session.*pam_unix.so.*/s//&\nsession	required	pam_mkhomedir.so skel=\/etc\/skel\/ umask=0022/" /etc/pam.d/common-session

else
  echo
  echo "We have a problem with PAM!"

fi

# Setting domain as NTP server:
add_to_ntp_config="
# Domain's NTP server
server ${domain}"

if [[ ${os} == "Ubuntu / Debian" || ${os} == "CentOS 7" ]]; then
  sed -i -e "/^server.*/s/^/#/" -e "/^pool.*/s/^/#/" /etc/ntp.conf
  echo "$add_to_ntp_config" >> /etc/ntp.conf

elif [[ ${os} == "CentOS 8" ]]; then
  sed -i -e "/^server.*/s/^/#/" -e "/^pool.*/s/^/#/" /etc/chrony.conf

else
  echo
  echo "We have a problem with NTP client!"

fi

# Adding NTP client to autostart and restarting it:
if [[ -e /etc/systemd && ${os} == "CentOS 7" ]]; then
  systemctl enable ntpd
  systemctl restart ntpd

elif [[ -e /etc/systemd && ${os} == "CentOS 8" ]]; then
  systemctl enable chronyd
  systemctl restart chronyd

elif [[ -e /etc/systemd && ${os} == "Ubuntu / Debian" ]]; then
  systemctl enable ntp
  systemctl restart ntp

elif [[ -e /etc/init.d/sssd ]]; then
  chkconfig ntpd on
  service ntpd restart

else
  echo
  echo "We have a problem with autostart for NTP client!"

fi

# Set domain group for sudoers.
echo
echo "----------------------------------------------------------"
echo "- Which domain security group are we providing sudo for? -"
echo "----------------------------------------------------------"
echo
echo "[1] - Sudoers-Dev"
echo "[2] - Sudoers-Java"
echo "[3] - Enter manually..."
echo

read -p "[1/2/3]: " groupselect

case "$groupselect" in

"1" )
  sudogroup="Sudoers-Dev"
  ;;

"2" )
  sudogroup="Sudoers-Java"
  ;;

"3" )
  echo
  read -p "Enter the security group name: " sudogroup
  ;;

* )
  echo
  echo "Heresy!"
  ;;

esac

add_group_to_sudoers="
## Allows people in group $sudogroup to run all commands
%$sudogroup     ALL=(ALL:ALL)     ALL"
echo "$add_group_to_sudoers" >> /etc/sudoers
